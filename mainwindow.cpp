#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    symbols=45000;
    a_plot.view=0x00;
    a_plot.divis=500;
    ui->setupUi(this);
    qRegisterMetaType<double>("double");
    qRegisterMetaType<QVector<double> >("QVector<double>");

    connect(&a_plot, SIGNAL(signaldraw(QVector<double>, QVector<double>)), this, SLOT(qwt_draw(QVector<double>, QVector<double>)));

    ss=ui->spinBox_sym_size->value();
    curve_1 = new QwtPlotCurve("Curve 1");
    if(ui->radioButton_none->isChecked()==true)
        curve_1->setStyle(QwtPlotCurve::NoCurve);
    if(ui->radioButton_lines->isChecked()==true)
         curve_1->setStyle(QwtPlotCurve::Lines);
    if(ui->radioButton_sticks->isChecked()==true)
         curve_1->setStyle(QwtPlotCurve::Sticks);

    curve_1->setRenderHint(QwtPlotItem::RenderAntialiased, true);
   curve_1->setPen(QColor(Qt::red));
   curve_1->setPen(QColor(Qt::red));
    curve_1->attach(ui->qwtPlot);
    ui->qwtPlot->setCanvasBackground(Qt::gray);

    symbol_1 = new QwtSymbol;
    symbol_1->setStyle(QwtSymbol::Ellipse);
    symbol_1->setSize(ss,ss);
    symbol_1->setPen(QColor(Qt::yellow));
    symbol_1->setBrush(QColor(Qt::yellow));
    curve_1->setSymbol(symbol_1);


    curve_2 = new QwtPlotCurve("Curve 2");
    if(ui->radioButton_none->isChecked()==true)
        curve_2->setStyle(QwtPlotCurve::NoCurve);
    if(ui->radioButton_lines->isChecked()==true)
         curve_2->setStyle(QwtPlotCurve::Lines);
    if(ui->radioButton_sticks->isChecked()==true)
         curve_2->setStyle(QwtPlotCurve::Sticks);

    curve_2->setRenderHint(QwtPlotItem::RenderAntialiased, true);
   curve_2->setPen(QColor(255,0,0));
   curve_2->setPen(QColor(Qt::darkRed));
    curve_2->attach(ui->qwtPlot);
    //ui->qwtPlot->setCanvasBackground(Qt::darkGray);

    symbol_2 = new QwtSymbol;
    symbol_2->setStyle(QwtSymbol::Ellipse);
    symbol_2->setSize(ss,ss);
    symbol_2->setPen(QColor(128,128,128));
    symbol_2->setBrush(QColor(128,128,128));
    curve_2->setSymbol(symbol_2);
    /*
    grid = new QwtPlotGrid();
    grid->enableXMin(true);
    grid->enableYMin(true);
    grid->setPen(QPen(Qt::black, 0, Qt::DotLine));
    grid->setPen(QPen(Qt::gray, 0 , Qt::DotLine));
    grid->attach(ui->qwtPlot);
*/



  //  ui->qwtPlot->enableAxis(QwtPlot::yLeft ,0);

	scaleX = new QwtPlotScaleItem();
    scaleX->setAlignment(QwtScaleDraw::BottomScale);
    ui->qwtPlot->enableAxis(QwtPlot::xBottom ,1);
    //scaleX->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-100,150, 10, 5));
    //scaleX->attach(ui->qwtPlot);
    scaleY = new QwtPlotScaleItem();
     ui->qwtPlot->setAxisScale(QwtPlot::yLeft, -150,150);  //-150,150
  //  scaleY->setAlignment(QwtScaleDraw::LeftScale);
   // scaleY->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-150,150,5,1)); //-150,150,10,5
	scaleY->attach(ui->qwtPlot);
    on_updateButton_clicked();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::qwt_draw(QVector<double> x, QVector<double> y)
{
   // int mply =2;

    if(ui->spinBox_lonib->value()==14)
    {
        scaleY->detach();



        QwtPlotMarker *qpsk1_4 =new QwtPlotMarker();
        qpsk1_4->setLabel(QString::fromLatin1("QPSK1/4"));
        qpsk1_4->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        qpsk1_4->setLabelOrientation(Qt::Horizontal);
        qpsk1_4->setLineStyle(QwtPlotMarker::HLine);
        qpsk1_4->setLinePen(Qt::blue,0,Qt::DotLine);
        qpsk1_4->setValue(0,-119);
        qpsk1_4->attach(ui->qwtPlot);

        QwtPlotMarker *qpsk1_3 =new QwtPlotMarker();
        qpsk1_3->setLabel(QString::fromLatin1("QPSK1/3"));
        qpsk1_3->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        qpsk1_3->setLabelOrientation(Qt::Horizontal);
        qpsk1_3->setLineStyle(QwtPlotMarker::HLine);
        qpsk1_3->setLinePen(Qt::blue,0,Qt::DotLine);
        qpsk1_3->setValue(0,-111);
        qpsk1_3->attach(ui->qwtPlot);

        QwtPlotMarker *qpsk2_5 =new QwtPlotMarker();
        qpsk2_5->setLabel(QString::fromLatin1("QPSK2/5"));
        qpsk2_5->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        qpsk2_5->setLabelOrientation(Qt::Horizontal);
        qpsk2_5->setLineStyle(QwtPlotMarker::HLine);
        qpsk2_5->setLinePen(Qt::blue,0,Qt::DotLine);
        qpsk2_5->setValue(0,-103);
        qpsk2_5->attach(ui->qwtPlot);

        QwtPlotMarker *qpsk1_2 =new QwtPlotMarker();
        qpsk1_2->setLabel(QString::fromLatin1("QPSK1/2"));
        qpsk1_2->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        qpsk1_2->setLabelOrientation(Qt::Horizontal);
        qpsk1_2->setLineStyle(QwtPlotMarker::HLine);
        qpsk1_2->setLinePen(Qt::blue,0,Qt::DotLine);
        qpsk1_2->setValue(0,-95);
        qpsk1_2->attach(ui->qwtPlot);

        QwtPlotMarker *qpsk3_5 =new QwtPlotMarker();
        qpsk3_5->setLabel(QString::fromLatin1("QPSK3/5"));
        qpsk3_5->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        qpsk3_5->setLabelOrientation(Qt::Horizontal);
        qpsk3_5->setLineStyle(QwtPlotMarker::HLine);
        qpsk3_5->setLinePen(Qt::blue,0,Qt::DotLine);
        qpsk3_5->setValue(0,-87);
        qpsk3_5->attach(ui->qwtPlot);

        QwtPlotMarker *qpsk2_3 =new QwtPlotMarker();
        qpsk2_3->setLabel(QString::fromLatin1("QPSK2/3"));
        qpsk2_3->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        qpsk2_3->setLabelOrientation(Qt::Horizontal);
        qpsk2_3->setLineStyle(QwtPlotMarker::HLine);
        qpsk2_3->setLinePen(Qt::blue,0,Qt::DotLine);
        qpsk2_3->setValue(0,-79);
        qpsk2_3->attach(ui->qwtPlot);

        QwtPlotMarker *qpsk3_4 =new QwtPlotMarker();
        qpsk3_4->setLabel(QString::fromLatin1("QPSK3/4"));
        qpsk3_4->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        qpsk3_4->setLabelOrientation(Qt::Horizontal);
        qpsk3_4->setLineStyle(QwtPlotMarker::HLine);
        qpsk3_4->setLinePen(Qt::blue,0,Qt::DotLine);
        qpsk3_4->setValue(0,-71);
        qpsk3_4->attach(ui->qwtPlot);

        QwtPlotMarker *qpsk4_5 =new QwtPlotMarker();
        qpsk4_5->setLabel(QString::fromLatin1("QPSK4/5"));
        qpsk4_5->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        qpsk4_5->setLabelOrientation(Qt::Horizontal);
        qpsk4_5->setLineStyle(QwtPlotMarker::HLine);
        qpsk4_5->setLinePen(Qt::blue,0,Qt::DotLine);
        qpsk4_5->setValue(0,-63);
        qpsk4_5->attach(ui->qwtPlot);

        QwtPlotMarker *qpsk5_6 =new QwtPlotMarker();
        qpsk5_6->setLabel(QString::fromLatin1("QPSK5/6"));
        qpsk5_6->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        qpsk5_6->setLabelOrientation(Qt::Horizontal);
        qpsk5_6->setLineStyle(QwtPlotMarker::HLine);
        qpsk5_6->setLinePen(Qt::blue,0,Qt::DotLine);
        qpsk5_6->setValue(0,-55);
        qpsk5_6->attach(ui->qwtPlot);

        QwtPlotMarker *qpsk8_9 =new QwtPlotMarker();
        qpsk8_9->setLabel(QString::fromLatin1("QPSK8/9"));
        qpsk8_9->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        qpsk8_9->setLabelOrientation(Qt::Horizontal);
        qpsk8_9->setLineStyle(QwtPlotMarker::HLine);
        qpsk8_9->setLinePen(Qt::blue,0,Qt::DotLine);
        qpsk8_9->setValue(0,-47);
        qpsk8_9->attach(ui->qwtPlot);

        QwtPlotMarker *qpsk9_10 =new QwtPlotMarker();
        qpsk9_10->setLabel(QString::fromLatin1("QPSK9/10"));
        qpsk9_10->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        qpsk9_10->setLabelOrientation(Qt::Horizontal);
        qpsk9_10->setLineStyle(QwtPlotMarker::HLine);
        qpsk9_10->setLinePen(Qt::blue,0,Qt::DotLine);
        qpsk9_10->setValue(0,-39);
        qpsk9_10->attach(ui->qwtPlot);

        QwtPlotMarker *psk_8_3_5 =new QwtPlotMarker();
        psk_8_3_5->setLabel(QString::fromLatin1("8PSK3/5"));
        psk_8_3_5->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        psk_8_3_5->setLabelOrientation(Qt::Horizontal);
        psk_8_3_5->setLineStyle(QwtPlotMarker::HLine);
        psk_8_3_5->setLinePen(Qt::blue,0,Qt::DotLine);
        psk_8_3_5->setValue(0,-31);
        psk_8_3_5->attach(ui->qwtPlot);

        QwtPlotMarker *psk_8_2_3 =new QwtPlotMarker();
        psk_8_2_3->setLabel(QString::fromLatin1("8PSK2/3"));
        psk_8_2_3->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        psk_8_2_3->setLabelOrientation(Qt::Horizontal);
        psk_8_2_3->setLineStyle(QwtPlotMarker::HLine);
        psk_8_2_3->setLinePen(Qt::blue,0,Qt::DotLine);
        psk_8_2_3->setValue(0,-23);
        psk_8_2_3->attach(ui->qwtPlot);

        QwtPlotMarker *psk_8_3_4 =new QwtPlotMarker();
        psk_8_3_4->setLabel(QString::fromLatin1("8PSK3/4"));
        psk_8_3_4->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        psk_8_3_4->setLabelOrientation(Qt::Horizontal);
        psk_8_3_4->setLineStyle(QwtPlotMarker::HLine);
        psk_8_3_4->setLinePen(Qt::blue,0,Qt::DotLine);
        psk_8_3_4->setValue(0,-15);
        psk_8_3_4->attach(ui->qwtPlot);

        QwtPlotMarker *psk_8_5_6 =new QwtPlotMarker();
        psk_8_5_6->setLabel(QString::fromLatin1("8PSK5/6"));
        psk_8_5_6->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        psk_8_5_6->setLabelOrientation(Qt::Horizontal);
        psk_8_5_6->setLineStyle(QwtPlotMarker::HLine);
        psk_8_5_6->setLinePen(Qt::blue,0,Qt::DotLine);
        psk_8_5_6->setValue(0,-7);
        psk_8_5_6->attach(ui->qwtPlot);

        QwtPlotMarker *psk_8_8_9 =new QwtPlotMarker();
        psk_8_8_9->setLabel(QString::fromLatin1("8PSK8/9"));
        psk_8_8_9->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        psk_8_8_9->setLabelOrientation(Qt::Horizontal);
        psk_8_8_9->setLineStyle(QwtPlotMarker::HLine);
        psk_8_8_9->setLinePen(Qt::blue,0,Qt::DotLine);
        psk_8_8_9->setValue(0,1);
        psk_8_8_9->attach(ui->qwtPlot);

        QwtPlotMarker *psk_8_9_10 =new QwtPlotMarker();
        psk_8_9_10->setLabel(QString::fromLatin1("8PSK9/10"));
        psk_8_9_10->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        psk_8_9_10->setLabelOrientation(Qt::Horizontal);
        psk_8_9_10->setLineStyle(QwtPlotMarker::HLine);
        psk_8_9_10->setLinePen(Qt::blue,0,Qt::DotLine);
        psk_8_9_10->setValue(0,9);
        psk_8_9_10->attach(ui->qwtPlot);

        QwtPlotMarker *apsk_16_2_3 =new QwtPlotMarker();
        apsk_16_2_3->setLabel(QString::fromLatin1("16APSK2/3"));
        apsk_16_2_3->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        apsk_16_2_3->setLabelOrientation(Qt::Horizontal);
        apsk_16_2_3->setLineStyle(QwtPlotMarker::HLine);
        apsk_16_2_3->setLinePen(Qt::blue,0,Qt::DotLine);
        apsk_16_2_3->setValue(0,17);
        apsk_16_2_3->attach(ui->qwtPlot);

        QwtPlotMarker *apsk_16_3_4 =new QwtPlotMarker();
        apsk_16_3_4->setLabel(QString::fromLatin1("16APSK3/4"));
        apsk_16_3_4->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        apsk_16_3_4->setLabelOrientation(Qt::Horizontal);
        apsk_16_3_4->setLineStyle(QwtPlotMarker::HLine);
        apsk_16_3_4->setLinePen(Qt::blue,0,Qt::DotLine);
        apsk_16_3_4->setValue(0,25);
        apsk_16_3_4->attach(ui->qwtPlot);

        QwtPlotMarker *apsk_16_4_5 =new QwtPlotMarker();
        apsk_16_4_5->setLabel(QString::fromLatin1("16APSK4/5"));
        apsk_16_4_5->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        apsk_16_4_5->setLabelOrientation(Qt::Horizontal);
        apsk_16_4_5->setLineStyle(QwtPlotMarker::HLine);
        apsk_16_4_5->setLinePen(Qt::blue,0,Qt::DotLine);
        apsk_16_4_5->setValue(0,33);
        apsk_16_4_5->attach(ui->qwtPlot);

        QwtPlotMarker *apsk_16_5_6 =new QwtPlotMarker();
        apsk_16_5_6->setLabel(QString::fromLatin1("16APSK5/6"));
        apsk_16_5_6->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        apsk_16_5_6->setLabelOrientation(Qt::Horizontal);
        apsk_16_5_6->setLineStyle(QwtPlotMarker::HLine);
        apsk_16_5_6->setLinePen(Qt::blue,0,Qt::DotLine);
        apsk_16_5_6->setValue(0,41);
        apsk_16_5_6->attach(ui->qwtPlot);

        QwtPlotMarker *apsk_16_8_9 =new QwtPlotMarker();
        apsk_16_8_9->setLabel(QString::fromLatin1("16APSK8/9"));
        apsk_16_8_9->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        apsk_16_8_9->setLabelOrientation(Qt::Horizontal);
        apsk_16_8_9->setLineStyle(QwtPlotMarker::HLine);
        apsk_16_8_9->setLinePen(Qt::blue,0,Qt::DotLine);
        apsk_16_8_9->setValue(0,49);
        apsk_16_8_9->attach(ui->qwtPlot);


        QwtPlotMarker *apsk_16_9_10 =new QwtPlotMarker();
        apsk_16_9_10->setLabel(QString::fromLatin1("16APSK9/10"));
        apsk_16_9_10->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        apsk_16_9_10->setLabelOrientation(Qt::Horizontal);
        apsk_16_9_10->setLineStyle(QwtPlotMarker::HLine);
        apsk_16_9_10->setLinePen(Qt::blue,0,Qt::DotLine);
        apsk_16_9_10->setValue(0,57);
        apsk_16_9_10->attach(ui->qwtPlot);


        QwtPlotMarker *apsk_32_3_4 =new QwtPlotMarker();
        apsk_32_3_4->setLabel(QString::fromLatin1("32APSK3/4"));
        apsk_32_3_4->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        apsk_32_3_4->setLabelOrientation(Qt::Horizontal);
        apsk_32_3_4->setLineStyle(QwtPlotMarker::HLine);
        apsk_32_3_4->setLinePen(Qt::blue,0,Qt::DotLine);
        apsk_32_3_4->setValue(0,65);
        apsk_32_3_4->attach(ui->qwtPlot);

        QwtPlotMarker *apsk_32_4_5 =new QwtPlotMarker();
        apsk_32_4_5->setLabel(QString::fromLatin1("32APSK4/5"));
        apsk_32_4_5->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        apsk_32_4_5->setLabelOrientation(Qt::Horizontal);
        apsk_32_4_5->setLineStyle(QwtPlotMarker::HLine);
        apsk_32_4_5->setLinePen(Qt::blue,0,Qt::DotLine);
        apsk_32_4_5->setValue(0,73);
        apsk_32_4_5->attach(ui->qwtPlot);

        QwtPlotMarker *apsk_32_5_6 =new QwtPlotMarker();
        apsk_32_5_6->setLabel(QString::fromLatin1("32APSK5/6"));
        apsk_32_5_6->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        apsk_32_5_6->setLabelOrientation(Qt::Horizontal);
        apsk_32_5_6->setLineStyle(QwtPlotMarker::HLine);
        apsk_32_5_6->setLinePen(Qt::blue,0,Qt::DotLine);
        apsk_32_5_6->setValue(0,81);
        apsk_32_5_6->attach(ui->qwtPlot);

        QwtPlotMarker *apsk_32_8_9 =new QwtPlotMarker();
        apsk_32_8_9->setLabel(QString::fromLatin1("32APSK8/9"));
        apsk_32_8_9->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        apsk_32_8_9->setLabelOrientation(Qt::Horizontal);
        apsk_32_8_9->setLineStyle(QwtPlotMarker::HLine);
        apsk_32_8_9->setLinePen(Qt::blue,0,Qt::DotLine);
        apsk_32_8_9->setValue(0,89);
        apsk_32_8_9->attach(ui->qwtPlot);

        QwtPlotMarker *apsk_32_9_10 =new QwtPlotMarker();
        apsk_32_9_10->setLabel(QString::fromLatin1("32APSK9/10"));
        apsk_32_9_10->setLabelAlignment(Qt::AlignCenter|Qt::AlignLeft);
        apsk_32_9_10->setLabelOrientation(Qt::Horizontal);
        apsk_32_9_10->setLineStyle(QwtPlotMarker::HLine);
        apsk_32_9_10->setLinePen(Qt::blue,0,Qt::DotLine);
        apsk_32_9_10->setValue(0,97);
        apsk_32_9_10->attach(ui->qwtPlot);

    }


	QVector<double> x_1;
    QVector<double> y_1;
    QVector<double> xy_1;

	
    for (int i = 0; i < x.size(); i++) {


        x_1.append(i);
      //  if(ui->checkBox_Isym->isChecked()==true)
            xy_1.append(x[i]);

    //    y_1.append(i);
     //       if(ui->checkBox_Qsym->isChecked()==true)
//                xy_1.append(y[i]);



    }


     //   if(ui->checkBox_Isym->isChecked()==true)
            curve_1->setSamples(x_1, xy_1);

   //     if(ui->checkBox_Qsym->isChecked()==true)
    //        curve_2->setSamples(y_1, xy_1 );


    ui->qwtPlot->replot();
    a_plot.setup(ui->adapterBox->currentText().toInt(), ui->loopBox->isChecked(), ui->spinBox_persistance->value(),ui->spinBox_magnifier->value());

}

void MainWindow::on_updateButton_clicked()
{
    cout << "on_pushButton_clicked()" << endl;
    a_plot.setup(ui->adapterBox->currentText().toInt(), ui->loopBox->isChecked(), ui->spinBox_persistance->value(),ui->spinBox_magnifier->value());
    ui->qwtPlot->setAxisScale(QwtPlot::xBottom, 0, symbols/ui->spinBox_magnifier->value());
    symbol_1->setSize(ss,ss);
    symbol_2->setSize(ss,ss);
    a_plot.view = ui->spinBox_hinib->value()<<4|ui->spinBox_lonib->value();

    a_plot.start();

}

void MainWindow::on_actionExit_triggered()
{
    a_plot.closeadapter();
    a_plot.quit();
    a_plot.wait();
    exit(1);
}

void MainWindow::on_spinBox_magnifier_valueChanged(int arg1)
{
    on_updateButton_clicked();
}


void MainWindow::on_spinBox_lonib_valueChanged(int arg1)
{
    int hinib = a_plot.view & 0xf0;
    a_plot.view = hinib | arg1;
    on_updateButton_clicked();
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
    int lonib = a_plot.view & 0x0f;
    a_plot.view = arg1 | lonib;
    on_updateButton_clicked();
}

void MainWindow::on_radioButton_none_clicked(bool checked)
{
    if(ui->radioButton_none->isChecked()==true)
        curve_1->setStyle(QwtPlotCurve::NoCurve);
        curve_2->setStyle(QwtPlotCurve::NoCurve);
   on_updateButton_clicked();
}

void MainWindow::on_radioButton_lines_clicked(bool checked)
{
    if(ui->radioButton_lines->isChecked()==true)
         curve_1->setStyle(QwtPlotCurve::Lines);
         curve_2->setStyle(QwtPlotCurve::Lines);
    on_updateButton_clicked();
}

void MainWindow::on_radioButton_sticks_clicked(bool checked)
{
    if(ui->radioButton_sticks->isChecked()==true)
         curve_1->setStyle(QwtPlotCurve::Sticks);
    on_updateButton_clicked();
}

void MainWindow::on_spinBox_div_by_valueChanged(int arg1)
{
    a_plot.divis=arg1;
    on_updateButton_clicked();
}

void MainWindow::on_spinBox_sym_size_valueChanged(int arg1)
{
    ss=ui->spinBox_sym_size->value();
    on_updateButton_clicked();
}
