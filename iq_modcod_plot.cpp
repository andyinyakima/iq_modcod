/*
 *
 * iq_mod_plot is written by
 * Andy Laberge andylaberge@linux.com
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "iq_modcod_plot.h"

string inttostring(int i)
{
    string s;
    stringstream out;
    out << i;
    s = out.str();

    return s;
}

void STV_plot::run()
{
    cout << "run()" << endl;

	x.clear();
	y.clear();
	int loop_counter = persistence;

	frontend_devname = "/dev/dvb/adapter" + inttostring(adapter) + "/frontend0";
    cout << "opening: " << frontend_devname << endl;
    if ((frontend_fd = open(frontend_devname.c_str(), O_RDONLY | O_NONBLOCK)) < 0)
    {
        cout << "failed to open " << frontend_devname << endl;
        return;
    }

	do {
		if (loop_counter)
			loop_counter--;
		sweep();
	} while (loop_counter || loop);

	close(frontend_fd);
}

void STV_plot::sweep()
{
   //int mode_select=0x00;
   //int iq_pick_point=0x0b;



    struct dvb_fe_constellation_samples const_samples;
    struct dvb_fe_constellation_sample samples[divis];
    const_samples.num = divis;
    const_samples.samples = samples;
    const_samples.options = view;

	if ((ioctl(frontend_fd, FE_GET_CONSTELLATION_SAMPLES, &const_samples)) == -1) {
		cout << "ERROR: FE_GET_CONSTELLATION_SAMPLES" << endl;
		return;
	}
	for (unsigned int i = 0 ; i < const_samples.num ; i++) {
        while (x.size() >= const_samples.num * persistence) {
			x.erase(x.begin());
			y.erase(y.begin());
		}
        x.append(samples[i].imaginary);
        y.append(samples[i].real);
	}
    emit signaldraw(x,y);

    cout << "sweep() complete" << endl;
}

void STV_plot::setup(int t_adapter, int t_loop, int t_persistence,int t_magnifier)
{
    adapter = t_adapter;
    loop = t_loop;
	persistence = t_persistence;
    magnifier = t_magnifier;

    cout << "setup() end" << endl;
}

void STV_plot::closeadapter()
{
}
