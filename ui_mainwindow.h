/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include "qwt_plot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionExit;
    QWidget *centralWidget;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout;
    QwtPlot *qwtPlot;
    QGridLayout *gridLayout_2;
    QRadioButton *radioButton_lines;
    QComboBox *adapterBox;
    QSpacerItem *verticalSpacer;
    QSpinBox *spinBox_magnifier;
    QLabel *label_3;
    QCheckBox *loopBox;
    QLabel *label_4;
    QPushButton *updateButton;
    QLabel *label;
    QLabel *label_5;
    QSpinBox *spinBox_sym_size;
    QSpinBox *spinBox_persistance;
    QSpinBox *spinBox_lonib;
    QSpinBox *spinBox_hinib;
    QLabel *label_2;
    QRadioButton *radioButton_sticks;
    QRadioButton *radioButton_none;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QSpinBox *spinBox_div_by;
    QCheckBox *checkBox_Isym;
    QCheckBox *checkBox_Qsym;
    QCheckBox *checkBox_IQsym;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuFile;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(871, 611);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(600, 300));
        MainWindow->setMaximumSize(QSize(2000, 1000));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_3 = new QGridLayout(centralWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        qwtPlot = new QwtPlot(centralWidget);
        qwtPlot->setObjectName(QStringLiteral("qwtPlot"));
        qwtPlot->setFrameShape(QFrame::StyledPanel);
        qwtPlot->setFrameShadow(QFrame::Raised);

        gridLayout->addWidget(qwtPlot, 0, 2, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        radioButton_lines = new QRadioButton(centralWidget);
        radioButton_lines->setObjectName(QStringLiteral("radioButton_lines"));
        radioButton_lines->setChecked(false);

        gridLayout_2->addWidget(radioButton_lines, 10, 1, 1, 1);

        adapterBox = new QComboBox(centralWidget);
        adapterBox->setObjectName(QStringLiteral("adapterBox"));

        gridLayout_2->addWidget(adapterBox, 0, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 14, 0, 1, 1);

        spinBox_magnifier = new QSpinBox(centralWidget);
        spinBox_magnifier->setObjectName(QStringLiteral("spinBox_magnifier"));
        spinBox_magnifier->setMinimum(1);
        spinBox_magnifier->setMaximum(1000);
        spinBox_magnifier->setValue(100);

        gridLayout_2->addWidget(spinBox_magnifier, 4, 1, 1, 1);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_3, 2, 0, 1, 1);

        loopBox = new QCheckBox(centralWidget);
        loopBox->setObjectName(QStringLiteral("loopBox"));
        loopBox->setLayoutDirection(Qt::RightToLeft);
        loopBox->setChecked(false);

        gridLayout_2->addWidget(loopBox, 3, 0, 1, 1);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_2->addWidget(label_4, 5, 0, 1, 1, Qt::AlignRight);

        updateButton = new QPushButton(centralWidget);
        updateButton->setObjectName(QStringLiteral("updateButton"));

        gridLayout_2->addWidget(updateButton, 3, 1, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label, 4, 0, 1, 1);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_5, 0, 0, 1, 1);

        spinBox_sym_size = new QSpinBox(centralWidget);
        spinBox_sym_size->setObjectName(QStringLiteral("spinBox_sym_size"));
        spinBox_sym_size->setEnabled(true);
        spinBox_sym_size->setMinimum(1);
        spinBox_sym_size->setMaximum(10);
        spinBox_sym_size->setValue(2);

        gridLayout_2->addWidget(spinBox_sym_size, 5, 1, 1, 1);

        spinBox_persistance = new QSpinBox(centralWidget);
        spinBox_persistance->setObjectName(QStringLiteral("spinBox_persistance"));
        spinBox_persistance->setMinimum(1);
        spinBox_persistance->setMaximum(45);
        spinBox_persistance->setValue(1);

        gridLayout_2->addWidget(spinBox_persistance, 2, 1, 1, 1);

        spinBox_lonib = new QSpinBox(centralWidget);
        spinBox_lonib->setObjectName(QStringLiteral("spinBox_lonib"));
        spinBox_lonib->setEnabled(false);
        spinBox_lonib->setMaximum(15);
        spinBox_lonib->setValue(14);

        gridLayout_2->addWidget(spinBox_lonib, 7, 1, 1, 1);

        spinBox_hinib = new QSpinBox(centralWidget);
        spinBox_hinib->setObjectName(QStringLiteral("spinBox_hinib"));
        spinBox_hinib->setEnabled(false);
        spinBox_hinib->setMaximum(7);

        gridLayout_2->addWidget(spinBox_hinib, 6, 1, 1, 1);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_2, 7, 0, 1, 1);

        radioButton_sticks = new QRadioButton(centralWidget);
        radioButton_sticks->setObjectName(QStringLiteral("radioButton_sticks"));
        radioButton_sticks->setChecked(false);

        gridLayout_2->addWidget(radioButton_sticks, 12, 1, 1, 1);

        radioButton_none = new QRadioButton(centralWidget);
        radioButton_none->setObjectName(QStringLiteral("radioButton_none"));
        radioButton_none->setChecked(true);

        gridLayout_2->addWidget(radioButton_none, 9, 1, 1, 1);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_6, 6, 0, 1, 1);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_7, 9, 0, 1, 1);

        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_8, 1, 0, 1, 1);

        spinBox_div_by = new QSpinBox(centralWidget);
        spinBox_div_by->setObjectName(QStringLiteral("spinBox_div_by"));
        spinBox_div_by->setMinimum(25);
        spinBox_div_by->setMaximum(1000);
        spinBox_div_by->setSingleStep(25);
        spinBox_div_by->setValue(500);

        gridLayout_2->addWidget(spinBox_div_by, 1, 1, 1, 1);

        checkBox_Isym = new QCheckBox(centralWidget);
        checkBox_Isym->setObjectName(QStringLiteral("checkBox_Isym"));
        checkBox_Isym->setChecked(true);

        gridLayout_2->addWidget(checkBox_Isym, 13, 0, 1, 1);

        checkBox_Qsym = new QCheckBox(centralWidget);
        checkBox_Qsym->setObjectName(QStringLiteral("checkBox_Qsym"));
        checkBox_Qsym->setEnabled(false);
        checkBox_Qsym->setChecked(false);

        gridLayout_2->addWidget(checkBox_Qsym, 13, 1, 1, 1);

        checkBox_IQsym = new QCheckBox(centralWidget);
        checkBox_IQsym->setObjectName(QStringLiteral("checkBox_IQsym"));
        checkBox_IQsym->setEnabled(false);
        checkBox_IQsym->setChecked(false);

        gridLayout_2->addWidget(checkBox_IQsym, 12, 0, 1, 1);


        gridLayout->addLayout(gridLayout_2, 0, 1, 1, 1);


        gridLayout_3->addLayout(gridLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 871, 27));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionExit);

        retranslateUi(MainWindow);
        QObject::connect(updateButton, SIGNAL(clicked()), MainWindow, SLOT(on_updateButton_clicked()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "IQ_ModCods", 0));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0));
        radioButton_lines->setText(QApplication::translate("MainWindow", "Lines", 0));
        adapterBox->clear();
        adapterBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "0", 0)
         << QApplication::translate("MainWindow", "1", 0)
         << QApplication::translate("MainWindow", "2", 0)
         << QApplication::translate("MainWindow", "3", 0)
         << QApplication::translate("MainWindow", "4", 0)
         << QApplication::translate("MainWindow", "5", 0)
         << QApplication::translate("MainWindow", "6", 0)
         << QApplication::translate("MainWindow", "7", 0)
         << QApplication::translate("MainWindow", "8", 0)
        );
        label_3->setText(QApplication::translate("MainWindow", "Persistence", 0));
        loopBox->setText(QApplication::translate("MainWindow", "Loop", 0));
        label_4->setText(QApplication::translate("MainWindow", "Symbol Size", 0));
        updateButton->setText(QApplication::translate("MainWindow", "Update", 0));
        label->setText(QApplication::translate("MainWindow", "X-zoom", 0));
        label_5->setText(QApplication::translate("MainWindow", "Adapter", 0));
        spinBox_persistance->setSuffix(QApplication::translate("MainWindow", " Samples", 0));
        label_2->setText(QApplication::translate("MainWindow", "LoNib", 0));
        radioButton_sticks->setText(QApplication::translate("MainWindow", "Sticks", 0));
        radioButton_none->setText(QApplication::translate("MainWindow", "NoConnect", 0));
        label_6->setText(QApplication::translate("MainWindow", "HiNib", 0));
        label_7->setText(QApplication::translate("MainWindow", "Connections", 0));
        label_8->setText(QApplication::translate("MainWindow", "Sample Size", 0));
        spinBox_div_by->setSuffix(QApplication::translate("MainWindow", " Points", 0));
        checkBox_Isym->setText(QApplication::translate("MainWindow", "I-Symbol", 0));
        checkBox_Qsym->setText(QApplication::translate("MainWindow", "Q-Symbol", 0));
        checkBox_IQsym->setText(QApplication::translate("MainWindow", "IQ-Symbols", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
