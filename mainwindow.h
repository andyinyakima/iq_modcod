#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_symbol.h>
#include <qwt_plot_scaleitem.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_marker.h>
#include <qwt_scale_engine.h>
#include "iq_modcod_plot.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int magnifier;
    int symbols;

public slots:
    void on_updateButton_clicked();
    void qwt_draw(QVector<double> x, QVector<double> y);

private slots:
    void on_actionExit_triggered();

    void on_spinBox_magnifier_valueChanged(int arg1);

    void on_spinBox_lonib_valueChanged(int arg1);

    void on_spinBox_valueChanged(int arg1);

    void on_radioButton_none_clicked(bool checked);

    void on_radioButton_lines_clicked(bool checked);

    void on_radioButton_sticks_clicked(bool checked);

    void on_spinBox_div_by_valueChanged(int arg1);

    void on_spinBox_sym_size_valueChanged(int arg1);

private:
    Ui::MainWindow *ui;
    STV_plot a_plot;
    QwtPlotGrid *grid;
    QwtPlotCurve *curve_1, *curve_2;
	QwtPlotScaleItem *scaleX;
	QwtPlotScaleItem *scaleY;
    QwtSymbol *symbol_1,*symbol_2;
    int ss;     // symbol size
};

#endif // MAINWINDOW_H
