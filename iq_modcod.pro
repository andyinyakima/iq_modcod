#-------------------------------------------------
#
# Project created by QtCreator 2015-05-25T16:07:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = iq_modcod
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp
SOURCES += iq_modcod_plot.cpp

HEADERS  += mainwindow.h
HEADERS += iq_modcod_plot.h


FORMS    += mainwindow.ui

INCLUDEPATH += /usr/local/qwt-6.1.3-svn/include
INCLUDEPATH += /usr/include/qwt
LIBS += -Wl,-rpath,/usr/local/qwt-6.1.3-svn/lib -L /usr/local/qwt-6.1.3-svn/lib -l:libqwt.so




